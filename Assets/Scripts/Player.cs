﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    float playerSpeed = 5;
    public float mouseSpeed = 5;
    int s;//current state as int
    int prevstate;//previous state as int
    CharacterController cc;
    float h;
    float v;
    float vlook;
    float hrot;
    Transform cam;

    Vector3 movedir;
    public enum PlayerStates
    {
        None,
        Attack,
        Duck
    }

    public PlayerStates playerstate;

	void Awake()
	{
        cc = this.transform.GetComponent<CharacterController>();
	}

	// Start is called before the first frame update
	void Start()
    {
        Cursor.visible = false;
        cam = this.transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {

        v = Input.GetAxis("Vertical");
        h = Input.GetAxis("Horizontal");
        vlook = Input.GetAxis("Mouse Y")*(Time.deltaTime*mouseSpeed);
        hrot = Input.GetAxis("Mouse X") * (Time.deltaTime * mouseSpeed);
        movedir.z = v;
        movedir.x = h;
                
        cc.transform.Rotate(0, hrot, 0, Space.Self);
        cam.Rotate(-vlook, 0, 0, Space.Self);

        //clamp looking up and down
        Vector3 camrot = cam.transform.eulerAngles;
        
        if (camrot.x > 180.0f)

        {

            camrot.x -= 360.0f;

        }

        camrot.x = Mathf.Clamp(camrot.x, -70.0f, 70.0f);

        cam.transform.eulerAngles = camrot;

        movedir = transform.TransformDirection(movedir);//translate to player's local axis space

        movedir *= playerSpeed;
        movedir *= Time.deltaTime;

        cc.Move(movedir);

        switch (playerstate)
        {
            case (PlayerStates.None):


                break;

            case (PlayerStates.Attack):

                if (prevstate != (int)PlayerStates.Attack)
                {
                    //do transition once;
                    print("S: ATTACK");
                }

                prevstate = (int)PlayerStates.Attack;



                break;

            case (PlayerStates.Duck):

                if (prevstate != (int)PlayerStates.Duck)
                {
                    //do transition once;
                    print("S:DUCK");

                }

                prevstate = (int)PlayerStates.Duck;

                break;

        }
    }

}
