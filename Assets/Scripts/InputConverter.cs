﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputConverter : MonoBehaviour
{
	float v;
	float h;
	CharacterController cc;
	Vector3 joypadVector;
	public TMP_Text joypadtext;
	public TMP_Text outputtext;
	public Transform cam;
	public Transform offsetObject;
	Vector3 offsetPos = Vector3.zero;
	
	public Transform camHelper;

	// Start is called before the first frame update
	void Start()
    {
		cc = this.transform.GetComponent<CharacterController>();
		offsetObject.position = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
		LedgeMovement();
		
		//v = Input.GetAxis("Vertical");
		//joypadVector.z = v;
		//joypadtext.text = "joypad vector: " + joypadVector;
		
		//joypadVector = transform.TransformDirection(joypadVector);

		//outputtext.text = "translated vector: " + Vector3.Angle(this.transform.forward,cam.forward);
		//joypadVector *= Time.deltaTime;
		//cc.Move(joypadVector);
    }

	void LedgeMovement()
	{
		Vector3 ledgeVector = Vector3.zero; 

		v = Input.GetAxis("Vertical");
		h = Input.GetAxis("Horizontal");

		Vector3 camrot = cam.rotation.eulerAngles;
		camrot.x = 0;		
		camHelper.eulerAngles = camrot;

		ledgeVector.z = v;
		ledgeVector.x = h;
		
		ledgeVector = camHelper.transform.TransformDirection(ledgeVector);

		offsetObject.position = this.transform.position + ledgeVector + new Vector3(0,2,0);

		CheckLedgeCollision();
		
	}

	void CheckLedgeCollision()
	{
		RaycastHit hit;

		if (Physics.Raycast(offsetObject.position, -transform.up, out hit, 2.0f,  LayerMask.GetMask("ledgenav")))
		{
			LedgeMovement( hit.collider.gameObject.name);
		}
	}

	void LedgeMovement(string s)
	{

		Vector3 ledgeDir;

		switch (s)
		{
			case "LedgeMoveRight":

				ledgeDir = new Vector3(1, 0, 0);
				break;

			case "LedgeMoveLeft":
				ledgeDir = new Vector3(-1, 0, 0);
				break;
			
			default:
				ledgeDir = new Vector3(0, 0, 0);
				break;
				
		}

		ledgeDir = transform.TransformDirection(ledgeDir);//nope, camera rotation?

		cc.Move(ledgeDir*Time.deltaTime);
	}
}
